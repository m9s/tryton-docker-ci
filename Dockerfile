from python:3.10

#COPY hgrc /etc/mercurial/hgrc
RUN pip install --no-cache-dir \
    #mercurial \
    #hg-evolve \
    'flake8 < 6.0.0' \
    isort \
    twine
